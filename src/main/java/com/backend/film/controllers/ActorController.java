package com.backend.film.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import com.backend.film.repositories.ActorRepo;


@RestController
public class ActorController {
	
	@Autowired
    ActorRepo actorRepository;


	@DeleteMapping("/api/deleteactor/{id}")
    public void delete(@PathVariable Long id) {
		 actorRepository.deleteActorFilms(id);
		 actorRepository.deleteById(id) ;
    }
	
}
