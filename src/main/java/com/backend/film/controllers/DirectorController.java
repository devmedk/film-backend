package com.backend.film.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import com.backend.film.repositories.DirectorRepo;


@RestController
public class DirectorController {
	
	@Autowired
    DirectorRepo directorRepository;


	@DeleteMapping("/api/deletedirector/{id}")
    public void delete(@PathVariable Long id) {
		directorRepository.deleteDirectorFilms(id);
		directorRepository.deleteById(id) ;
    }
	
}
