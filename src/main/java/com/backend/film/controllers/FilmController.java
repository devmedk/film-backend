package com.backend.film.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import com.backend.film.repositories.FilmRepo;


@RestController
public class FilmController {
	
	@Autowired
    FilmRepo filmRepository;


	@DeleteMapping("/api/deletefilm/{id}")
    public void delete(@PathVariable Long id) {
		
		//if a specific film is deleted the film is deteched from the film_actor and film_director
		 filmRepository.deleteById(id) ;
		 
    }
	
}
