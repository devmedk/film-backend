package com.backend.film.entities;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;

@Entity
public class Film implements Serializable {

	/**
	 * 
	 */

	private static final long serialVersionUID = 1L;

	public Film(String title, HashSet<Director> directors, HashSet<Actor> actors) {
		// TODO Auto-generated constructor stub
		this.title = title;
		this.actors = actors;
		this.directors = directors;
	}

	public Film() {
		super();
		// TODO Auto-generated constructor stub
	}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;
	private String image;
	private String title;

	@ManyToMany(cascade = { CascadeType.PERSIST, CascadeType.DETACH })
	@JoinTable(name = "film_actor", joinColumns = { @JoinColumn(name = "film_id") }, inverseJoinColumns = {
			@JoinColumn(name = "actor_id") })
	private Set<Actor> actors = new HashSet<>();

	
	@ManyToMany(cascade = { CascadeType.PERSIST, CascadeType.DETACH })
	@JoinTable(
			name = "film_director", 
			joinColumns = @JoinColumn(name = "film_id"), 
			inverseJoinColumns = @JoinColumn(name = "director_id")
	    )
	private Set<Director> directors = new HashSet<>();

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getImage() {
		return image;
	}

	public void setImage(String image) {
		this.image = image;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public Set<Actor> getActors() {
		return actors;
	}

	public void setActors(Set<Actor> actors) {
		this.actors = actors;
	}

	public Set<Director> getDirectors() {
		return directors;
	}

	public void setDirectors(Set<Director> directors) {
		this.directors = directors;
	}

}
