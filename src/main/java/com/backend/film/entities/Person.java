package com.backend.film.entities;

public interface Person {
	String getFirstName() ;
	
	String getLastName() ;
	
	String getImage() ;
}
