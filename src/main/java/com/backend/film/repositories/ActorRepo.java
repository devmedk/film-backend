package com.backend.film.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.transaction.annotation.Transactional;

import com.backend.film.entities.Actor;


@RepositoryRestResource(collectionResourceRel = "actors", path = "actors")
public interface ActorRepo extends JpaRepository<Actor, Long> {
	
	@Query("Select a from Actor a where a.firstName like %:name% or a.lastName like %:name% " )
	List<Actor> findByNameContaining(@Param("name") String name);
	


	
	@Modifying
	@Transactional
	@Query(value="DELETE FROM film_actor WHERE actor_id=:id", nativeQuery = true)
	void deleteActorFilms(@Param("id") long id );
	
	
	// FIXME full name search
		// https://stackoverflow.com/questions/4732955/java-jpa-search-user-in-database-by-first-and-last-name-return-too-many-resul?rq=1
		

	

}
