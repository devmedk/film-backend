package com.backend.film.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.transaction.annotation.Transactional;

import com.backend.film.entities.Director;

@RepositoryRestResource(collectionResourceRel = "directors", path = "directors")
public interface DirectorRepo extends JpaRepository<Director, Long>{
	
	@Query("Select d from Director d where d.firstName like %:name% or d.lastName like %:name%" )
	List<Director> findByNameContaining(@Param("name") String name);
	


	@Modifying
	@Transactional
	@Query(value="DELETE FROM film_director WHERE director_id=:id", nativeQuery = true)
	void deleteDirectorFilms(@Param("id") long id );
	
	
	// FIXME full name search
	// https://stackoverflow.com/questions/4732955/java-jpa-search-user-in-database-by-first-and-last-name-return-too-many-resul?rq=1


}
