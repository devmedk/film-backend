package com.backend.film.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.backend.film.entities.Film;


@RepositoryRestResource(collectionResourceRel = "films", path = "films")
public interface FilmRepo extends JpaRepository<Film, Long> {
	List<Film> findByTitleContaining(@Param("title") String title);
	
	@Query(value="Delete from Film_Actor where film_id=:id", nativeQuery = true)
	 void deleteById(@Param("id") long id);
	 
}
